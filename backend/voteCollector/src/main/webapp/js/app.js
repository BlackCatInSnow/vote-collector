﻿/// <reference path="oidc-client.js" />

let config = {
    authority: 'http://39.108.185.8:5000/',
    client_id: 'voteCollector',
    redirect_uri: 'http://localhost:5003/callback.html',
    response_type: 'id_token token',
    scope: 'openid profile phone email address',
};

const mgr = new Oidc.UserManager(config)

document.getElementById("login").addEventListener("click", login, false);
document.getElementById("signup").addEventListener("click", signup, false);
document.getElementById("logout").addEventListener("click", logout, false);

mgr.getUser().then(function (user) {
    if (user) {
        let profile = user.profile;
        profile.session_state = user.session_state;
        profile.expires_at = user.expires_at;
        console.log("用户已登录" + user.profile);
        fetch("login",{
            headers:{
                "Content-Type": "application/json"
            },
            method:'post',
            body:JSON.stringify(user.profile)
        })
            .then((respone)=>{console.log(respone);})
            .catch((e)=>{console.log(e);})
    }
    else {
        log("用户未登录");
    }
});

mgr.even.

// 当检测用户退出系统时，强行退出
window.onmessage = function (e) {
    this.console.log(e.data)
    if (e.data === "change") mgr.removeUser();
}

function login() {
    mgr.signinRedirect();
}

function logout() {
    mgr.signoutRedirect();
}

function signup() {
    window.location.href = config.authority + "account/signup";
}