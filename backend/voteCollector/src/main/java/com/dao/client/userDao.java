package com.dao.client;

import com.entity.user;
import com.entity.userClaim;

import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/17 16:39
 **/
public interface userDao {

    /**
     * 查找 sid 在用户表是否存
     * @param sid 用户唯一特征 ID
     * @return int
     */
    int findUserBySid(String sid);

    /**
     * 更新用户数据
     * @param uc 一个 userClaim 对象
     * @return int
     */
    int updateUser(userClaim uc);

    /**
     * 向user表插入新的用户数据
     *
     * @param uc 一个 userClaim 对象
     * @return int
     */
    int insertUser(userClaim uc);

    /**
     *
     * @param uc
     * @return
     */
    int insertUserInfo(userClaim uc);
}
