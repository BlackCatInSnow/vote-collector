package com.dao.client;

import com.entity.option;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/19 17:24
 **/
public interface optionDao {
    int insertOption(option option);

    int isOptionIdExist(option option);

    int updateNumberByOptionId(int optionId);

    List<option> findOptionByTitleId(int fk_title);
}
