package com.dao.client;

import com.entity.voteDocument;

import java.util.List;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/18 17:47
 **/
public interface voteDocumentDao {

    /**
     * 通过用户id和标题内容查询出指定标题的id
     * userId   content
     *
     * @param map
     * @return
     */
    int findHeadlineIdByUserIdContent(Map<String, Object> map);

    /**
     * 插入一条数据：投票标题
     * content userId createTime deadline
     *
     * @param voteDocument
     * @return
     */
    int insertVoteDocument(voteDocument voteDocument);

    /**
     * 通过用户id和标题内容查询出指定标题的id
     * 是否存在
     * userId  content
     *
     * @param map
     * @return
     */
    int isHeadlineIdExist(Map<String, Object> map);

    /**
     * 修改投票的状态
     * 将投票的状态修改为0：已删除
     *
     * @param phone
     * @return
     */
    int updateVoteDocumentStatus(int headlineId);

    /**
     * 查询数据库所有投票的标题
     *
     * @return
     */
    List<voteDocument> findAllVoteDocument(Map<String, Integer> map);

    /**
     * 查询数据库中投票的数目
     *
     * @param map
     * @return
     */
    int getCountVoteDocument(Map<String, Integer> map);

    int findEnabled(int headlineId);

    /**
     * 更改投票的启用字段，截止日期
     *
     * @param map
     * @return
     */
    int updateDeadlineEnable(Map<String, Object> map);

    /**
     * 根据headlineId值查询出指定的投票标题
     * @param headlineId
     * @return
     */
    voteDocument findVoteDocumentByHeadline(int headlineId);

    /**
     * 真的删除数据库中的投票
     * @param headlineId
     * @return
     */
    int deleteVoteDocumentByHeadlineId(int headlineId);
}
