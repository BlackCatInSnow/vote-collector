package com.dao.client;

import com.entity.vote;

import java.util.List;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/19 09:29
 **/
public interface voteDao {
    /**
     * 插入一条数据（题目）
     * titleContent题目内容  headlineId标题id
     *
     * @param vote
     * @return
     */
    int insertVote(vote vote);

    /**
     * 通过标题id和题目内容
     * 查询出指定题目的id值
     *
     * @param map titleContent headlineId
     * @return
     */
    int findTitleIdByTitleContentHeadlineId(Map<String, Object> map);

    /**
     * 通过标题id和题目内容
     * 判断该题目是否存在数据库中
     *
     * @param map
     * @return
     */
    int isTitleIdExist(Map<String, Object> map);

    /**
     * 根据headlineId值查询出指定标题id值的题目
     *
     * @param deadlineId
     * @return
     */
    List<vote> findVoteByHeadlineId(int headlineId);
}
