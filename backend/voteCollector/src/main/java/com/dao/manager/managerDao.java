package com.dao.manager;

import com.entity.user;

import java.util.List;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/22 20:46
 **/
public interface managerDao {

    /**
     * 登录
     * 通过手机号和密码查找是否存在该管理员
     *
     * @param map
     * @return
     */
    int findManagerByPhonePwd(Map<String, String> map);

    /**
     * 注册
     *
     * @param user
     * @return
     */
    int insertManager(user user);

    /**
     * 查询所有用户
     * @return
     */
    List<user> findAllUser();
}
