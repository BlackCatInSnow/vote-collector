package com.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/22 09:40
 * 将index,pageSize封装成Map集合结构
 **/
public class pageIndex {
    public static Map<String,Integer> getMap(int index,int pageSize) {
        Map<String,Integer> map=new HashMap<>();
        map.put("index",(index-1)*pageSize);
        map.put("pageSize",pageSize);
        return map;
    }
}
