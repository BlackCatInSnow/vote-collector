package com.util;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author CAIYUHUI
 * @create 2018/07/23 10:59
 **/
@WebFilter(filterName = "corsFilter", urlPatterns = {"/*"})
public class corsFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletResponse resp=(HttpServletResponse)response;
        resp.setHeader("Access-Control-Allow-Origin","*");
        resp.setHeader("Access-Control-Allow-Methods","*");
        resp.setHeader("Access-Control-Allow-Headers","Content-Type,XFILENAME,XFILECATEGORY,XFILESIZE");
        response.setCharacterEncoding("utf-8");
        HttpServletRequest req=(HttpServletRequest)request;
        req.setCharacterEncoding("utf-8");
        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
