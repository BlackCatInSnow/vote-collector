package com.util;

import com.entity.userClaim;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @author CAIYUHUI
 * @create 2018/07/27 02:01
 **/
@WebFilter(filterName = "sidFilter", urlPatterns = {""})
public class sidFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        String sid = request.getParameter("sid");
        if (sid!=null) {
            userClaim claim=new userClaim();
            claim.setSid(sid);
        }
        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
