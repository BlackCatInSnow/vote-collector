package com.util;

import com.entity.userClaim;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Map;

public  class parseUserClaimJson {
    public static userClaim getUserClaim(String jsonString) throws IOException {
        Map map = new ObjectMapper().readValue(jsonString,Map.class);
        userClaim uc = new userClaim();
        uc.setSid(map.get("sid").toString());
        uc.setPreferred_username(map.get("preferred_username").toString());
        uc.setName(map.get("name").toString());
        uc.setPhone_number(map.get("phone_number").toString());
        uc.setEmail(map.get("email").toString());
        uc.setSession_state(map.get("session_state").toString());
        uc.setExpires_at(map.get("expires_at").toString());
        return uc;
    }
}
