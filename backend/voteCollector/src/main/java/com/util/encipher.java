package com.util;

import sun.misc.BASE64Encoder;

import java.security.MessageDigest;

/**
 * @author CAIYUHUI
 * @create 2018/07/18 16:23
 * 该工具类的功能是
 *  对传入的参数通过MD5加密方式加密后，返回加密后的字符串
 **/
public class encipher {
    public static String encodingByMD5(String str) {
        try {
            MessageDigest md5=MessageDigest.getInstance("MD5"); //采用MD5加密方式
            byte[] digest = md5.digest(str.getBytes("utf-8"));
            BASE64Encoder base64Encoder = new BASE64Encoder();
            String newStr = base64Encoder.encode(digest);  //二进制转字符串
            return newStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
