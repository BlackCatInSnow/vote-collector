package com.entity;

/**
 * @author CAIYUHUI
 * @create 2018/07/17 11:54
 * 选项表
 **/
public class option {
    private int optionId;
    private String optionContent;
    private int number;
    private int fk_title;

    /**
     * 选项Id
     *
     * @return
     */
    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

    /**
     * 选项的内容
     *
     * @return
     */
    public String getOptionContent() {
        return optionContent;
    }

    public void setOptionContent(String optionContent) {
        this.optionContent = optionContent;
    }

    /**
     * 该选项的投票数
     *
     * @return
     */
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getFk_title() {
        return fk_title;
    }

    public void setFk_title(int fk_title) {
        this.fk_title = fk_title;
    }

    @Override
    public String toString() {
        return "option{" +
                "optionId=" + optionId +
                ", optionContent='" + optionContent + '\'' +
                ", number=" + number +
                ", fk_title=" + fk_title +
                '}';
    }
}
