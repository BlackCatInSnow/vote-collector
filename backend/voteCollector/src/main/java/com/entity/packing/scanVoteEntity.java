package com.entity.packing;

/**
 * @author CAIYUHUI
 * @create 2018/07/26 15:52
 **/
public class scanVoteEntity {
    private int index;
    private int userId;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
