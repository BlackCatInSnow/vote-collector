package com.entity.packing;

import com.entity.voteDocument;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/21 15:26
 **/
public class voteEntity {
    public voteDocument voteDocument; //标题
    public List<titleEntity> titleEntitiesList; //所有题目

    public com.entity.voteDocument getVoteDocument() {
        return voteDocument;
    }

    public void setVoteDocument(com.entity.voteDocument voteDocument) {
        this.voteDocument = voteDocument;
    }

    public List<titleEntity> getTitleEntitiesList() {
        return titleEntitiesList;
    }

    public void setTitleEntitiesList(List<titleEntity> titleEntitiesList) {
        this.titleEntitiesList = titleEntitiesList;
    }

    @Override
    public String toString() {
        return "voteEntity{" +
                "voteDocument=" + voteDocument +
                ", titleEntitiesList=" + titleEntitiesList +
                '}';
    }
}
