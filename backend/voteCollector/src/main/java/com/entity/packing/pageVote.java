package com.entity.packing;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/26 10:48
 **/
public class pageVote<T> {
    private int currentIndex;
    private int pageSize;
    private int totalPage;
    private int totalNum;
    private List<T> voteEntity;

    /**
     * 当前是第几页
     *
     * @return
     */
    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    /**
     * 一页显示多少条数据
     *
     * @return
     */
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 总共有多少页
     *
     * @return
     */
    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * 总共有多少条数据
     *
     * @return
     */
    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    /**
     * 投票的内容
     *
     * @return
     */
    public List<T> getVoteEntity() {
        return voteEntity;
    }

    public void setVoteEntity(List<T> voteEntity) {
        this.voteEntity = voteEntity;
    }
}
