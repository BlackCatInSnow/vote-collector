package com.entity.packing;

/**
 * @author CAIYUHUI
 * @create 2018/07/26 13:59
 **/
public class enableParam {
    private int headlineId;
    private String deadline;

    public int getHeadlineId() {
        return headlineId;
    }

    public void setHeadlineId(int headlineId) {
        this.headlineId = headlineId;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}
