package com.entity.packing;

import com.entity.option;
import com.entity.vote;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/21 15:28
 **/
public class titleEntity {
    public vote vote; //题目
    public List<option> optionList; //所有选项

    public com.entity.vote getVote() {
        return vote;
    }

    public void setVote(com.entity.vote vote) {
        this.vote = vote;
    }

    public List<option> getOptionList() {
        return optionList;
    }

    public void setOptionList(List<option> optionList) {
        this.optionList = optionList;
    }

    @Override
    public String toString() {
        return "titleEntity{" +
                "vote=" + vote +
                ", optionList=" + optionList +
                '}';
    }
}
