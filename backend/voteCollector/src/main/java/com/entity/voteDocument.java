package com.entity;

/**
 * @author CAIYUHUI
 * @create 2018/07/17 11:18
 * 标题表
 **/
public class voteDocument {
    private int headlineId;
    private String content;
    private String createTime;
    private String deadline;
    private int voteStatus;
    private int fk_userId;
    private int enabled;


    /**
     * 标题Id
     *
     * @return
     */
    public int getHeadlineId() {
        return headlineId;
    }

    public void setHeadlineId(int headlineId) {
        this.headlineId = headlineId;
    }

    /**
     * 标题内容
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 投票创建时间
     *
     * @return
     */
    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * 投票截止日期
     *
     * @return
     */
    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    /**
     * 投票的状态
     * 0：已删除   1：正常
     *
     * @return
     */
    public int getVoteStatus() {
        return voteStatus;
    }

    public void setVoteStatus(int voteStatus) {
        this.voteStatus = voteStatus;
    }

    public int getFk_userId() {
        return fk_userId;
    }

    public void setFk_userId(int fk_userId) {
        this.fk_userId = fk_userId;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "voteDocument{" +
                "headlineId=" + headlineId +
                ", content='" + content + '\'' +
                ", createTime='" + createTime + '\'' +
                ", deadline='" + deadline + '\'' +
                ", voteStatus=" + voteStatus +
                ", fk_userId=" + fk_userId +
                ", enabled=" + enabled +
                '}';
    }
}
