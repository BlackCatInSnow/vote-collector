package com.entity;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/17 11:12
 * 用户表
 **/
public class user {
    private int userId;
    private String phone;
    private String password;
    private String username;
    private int role;
    private int status;
    private String registerTime; //注册时间
    /**
     * 用户Id
     *
     * @return
     */
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 手机号码
     *
     * @return
     */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 用户密码
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 用户昵称
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 角色
     * 0：普通用户；1：管理员；
     *
     * @return
     */
    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    /**
     * 状态
     * 1：正常；0：注销
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    @Override
    public String toString() {
        return "user{" +
                "userId=" + userId +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", role=" + role +
                ", status=" + status +
                ", registerTime=" + registerTime +
                '}';
    }
}
