package com.entity;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/17 11:26
 * 题目表
 **/
public class vote {
    private int titleId;
    private String titleContent;
    private int fk_headlineId;
    private int type;

    /**
     * 题目Id
     *
     * @return
     */
    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }

    /**
     * 题目的内容
     *
     * @return
     */
    public String getTitleContent() {
        return titleContent;
    }

    public void setTitleContent(String titleContent) {
        this.titleContent = titleContent;
    }

    public int getFk_headlineId() {
        return fk_headlineId;
    }

    public void setFk_headlineId(int fk_headlineId) {
        this.fk_headlineId = fk_headlineId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "vote{" +
                "titleId=" + titleId +
                ", titleContent='" + titleContent + '\'' +
                ", fk_headlineId=" + fk_headlineId +
                ", type=" + type +
                '}';
    }
}
