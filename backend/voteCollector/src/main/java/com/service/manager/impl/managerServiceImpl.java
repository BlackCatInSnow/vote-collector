package com.service.manager.impl;

import com.dao.manager.managerDao;
import com.entity.user;
import com.service.manager.managerService;
import com.util.encipher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/23 14:43
 **/
@Service("managerService")
public class managerServiceImpl implements managerService {

    @Autowired
    private managerDao managerDao;

    /**
     * 登录
     *
     * @param
     * @return
     */
    @Override
    public boolean login(String phone, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("phone", phone);
        map.put("password", encipher.encodingByMD5(password));
        int num = managerDao.findManagerByPhonePwd(map);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 注册
     *
     * @param
     * @return
     */
    @Override
    public boolean register(String phone, String password) {
        user user = new user();
        user.setPhone(phone);

        user.setPassword(encipher.encodingByMD5(password));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        user.setRegisterTime(sdf.format(new Date()));

        int num = managerDao.insertManager(user);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 查询所有用户
     * @return
     */
    @Override
    public List<user> findAllUser() {
        return managerDao.findAllUser();
    }
}
