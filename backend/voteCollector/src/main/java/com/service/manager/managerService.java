package com.service.manager;

import com.entity.user;

import java.util.List;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/23 09:36
 **/
public interface managerService {
    /**
     * 管理员登录
     * @param map
     * @return
     */
    boolean login(String phone,String password);

    /**
     * 注册管理员
     * @param map
     * @return
     */
    boolean register(String phone,String password);

    /**
     * 查询所有用户
     * @return
     */
    List<user> findAllUser();


}
