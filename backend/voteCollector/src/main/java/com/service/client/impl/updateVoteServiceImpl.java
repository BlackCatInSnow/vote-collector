package com.service.client.impl;

import com.dao.client.optionDao;
import com.dao.client.voteDocumentDao;
import com.service.client.updateVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/21 20:36
 **/
@Service("updateVoteService")
public class updateVoteServiceImpl implements updateVoteService {

    @Autowired
    private voteDocumentDao voteDocumentDao;

    @Autowired
    private optionDao optionDao;

    /**
     * 参与投票
     * 更新选项表中指定id值的选项number字段的值加1
     *
     * @param optionId
     * @return
     */
    @Override
    public boolean writeVote(int optionId) {
        int num = optionDao.updateNumberByOptionId(optionId);
        if (num>0) {
            return true;
        }
        return false;
    }

    /**
     * 删除投票
     * 修改投票的voteStatus字段值为0（1：正常；0：已删除）
     *
     * @param headlineId
     * @return
     */
    @Override
    public boolean deleteVote(int headlineId) {
        int num = voteDocumentDao.updateVoteDocumentStatus(headlineId);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 查看记录投票是否已启用字段
     * true：已启用；false：未启用
     *
     * @param headlineId
     * @return
     */
    @Override
    public boolean isVoteEnabled(int headlineId) {
        if (voteDocumentDao.findEnabled(headlineId) == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 启用投票
     * @param headlineId
     * @param deadline
     * @return
     */
    @Override
    public boolean updateEnable(int headlineId, String deadline) {
        Map<String,Object> map=new HashMap<>();
        map.put("headlineId",headlineId);
        map.put("deadline",deadline);
        int num = voteDocumentDao.updateDeadlineEnable(map);
        if (num>0) {
            return true;
        }
        return false;
    }

    /**
     * 删除数据库表中的投票
     * @param headlineId
     * @return
     */
    @Override
    public boolean delete(int headlineId) {
        int num = voteDocumentDao.deleteVoteDocumentByHeadlineId(headlineId);
        if (num>0) {
            return true;
        }
        return false;
    }
}
