package com.service.client.impl;

import com.dao.client.userDao;
import com.entity.user;
import com.entity.userClaim;
import com.service.client.userService;
import com.util.encipher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/18 11:18
 **/
@Service("userService")
public class userServiceImpl implements userService {

    @Autowired
    private userDao userDao;

    /**
     * 登录
     * @param uc 一个 userClaim 对象
     * @return true false
     */
    @Override
    public boolean login(userClaim uc) {
        boolean res;
        if (userDao.findUserBySid(uc.getSid()) > 0) res = userDao.updateUser(uc) > 0;
        else res = userDao.insertUser(uc) > 0 && userDao.insertUserInfo(uc) >0;
        return res;
    }

    /**
     * 用户注册
     * 通过手机号码和密码进行注册
     * 用户状态和角色字段默认分别为正常，普通用户
     *
     * @param user
     * @return
     */
    @Override
    public boolean register(user user) {
        //默认状态为正常，角色为普通用户
        user.setStatus(1);
        user.setRole(0);

        String password = user.getPassword();
        //对密码进行加密
        String str = encipher.encodingByMD5(password);
        user.setPassword(str);

        //注册时间为当前时间
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String curTime = sdf.format(new Date());
        user.setRegisterTime(curTime);

        int num = 0;
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 修改用户密码
     *
     * @param phone,password
     * @return
     */
    @Override
    public boolean updatePassword(String phone, String password) {
        //对密码进行加密操作后再存入数据库
        String str = encipher.encodingByMD5(password);
        user user = new user();
        user.setPhone(phone);
        user.setPassword(str);
        boolean flag = true;
        if (flag) {
            return true;
        }
        return false;
    }

    /**
     * 判断该手机号码是否已经注册
     *
     * @param phone
     * @return boolean
     */
    @Override
    public boolean isPhoneExist(String phone) {
        user user = findUserByPhone(phone);
        if (user != null) {
            return true;
        }
        return false;
    }

    /**
     * 通过手机号码查询出该用户信息
     *
     * @param phone
     * @return user
     */
    @Override
    public user findUserByPhone(String phone) {
        user user = null;
        if (user != null) {
            return user;
        }
        return null;
    }
}