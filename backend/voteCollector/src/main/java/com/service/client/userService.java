package com.service.client;

import com.entity.user;
import com.entity.userClaim;

import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/18 11:16
 **/
public interface userService {

    boolean login(userClaim uc);

    boolean register(user user);

    boolean updatePassword(String phone, String password);

    boolean isPhoneExist(String phone);

    user findUserByPhone(String phone);
}
