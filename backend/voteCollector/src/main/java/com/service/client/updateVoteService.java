package com.service.client;

/**
 * @author CAIYUHUI
 * @create 2018/07/21 20:22
 * 参与投票
 **/
public interface updateVoteService {

    /**
     * 参与投票
     *
     * @param optionId
     * @return
     */
    boolean writeVote(int optionId);

    /**
     * 删除投票
     *
     * @param headlineId
     * @return
     */
    boolean deleteVote(int headlineId);

    /**
     * 查看记录投票是否已启用字段
     * 1：已启用；0：未启用
     *
     * @param headlineId
     * @return
     */
    boolean isVoteEnabled(int headlineId);


    boolean updateEnable(int headlineId, String deadline);


    boolean delete(int headlineId);
}
