package com.service.client;

import com.entity.option;
import com.entity.packing.pageVote;
import com.entity.packing.voteEntity;
import com.entity.vote;
import com.entity.voteDocument;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/22 08:05
 **/
public interface findVoteService {

    List<voteEntity> findAllVote(int index, int pageSize);

    List<voteDocument> findVoteDocument(int index, int pageSize, int userId);

    List<vote> findVote(int headlineId);

    List<option> findOption(int titleId);

    List<voteEntity> findVoteByUserId(int index, int pageSize, int userId);

    pageVote toPageVote(int index, int pageSize, int userId);

    voteDocument findVoteDocumentByHeadlineId(int headlineId);

    voteEntity toVoteEntity(int headlineId);
}
