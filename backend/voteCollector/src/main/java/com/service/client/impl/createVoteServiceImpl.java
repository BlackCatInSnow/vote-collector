package com.service.client.impl;

import com.dao.client.optionDao;
import com.dao.client.voteDao;
import com.dao.client.voteDocumentDao;
import com.entity.option;
import com.entity.packing.titleEntity;
import com.entity.packing.voteEntity;
import com.entity.vote;
import com.entity.voteDocument;
import com.service.client.createVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/19 20:05
 **/
@Service("createVoteService")
public class createVoteServiceImpl implements createVoteService {

    @Autowired
    private voteDocumentDao voteDocumentDao;
    @Autowired
    private voteDao voteDao;
    @Autowired
    private optionDao optionDao;

    /**
     * 创建投票
     * @return
     */
    @Override
    public int createNewVote(voteEntity voteEntity) {
        //查询该标题是否已存在数据库中 参数：userId,content
        if (!isContentExist(voteEntity.getVoteDocument().getFk_userId(), voteEntity.getVoteDocument().getContent())) {
            //不存在，则插入标题
            insertVoteDocument(voteEntity.getVoteDocument());
        }

        //查询出该标题的id(headlineId) 参数userId,content
        int headlineId = findHeadlineId(voteEntity.getVoteDocument().getFk_userId(), voteEntity.getVoteDocument().getContent());

        //设置所有题目的标题id
        for (titleEntity titleEntity : voteEntity.getTitleEntitiesList()) {
            titleEntity.getVote().setFk_headlineId(headlineId);
            //判断该题目是否存在数据库中
            if (!isTitleContentExist(titleEntity.getVote().getTitleContent(), headlineId)) {
                //不存在，则将该题目插入数据库中
                insertVote(titleEntity.getVote());
            }
            //查询出题目的id值
            int titleId = findTitleId(titleEntity.getVote().getTitleContent(), headlineId);
            //为该题目的所有选项设置题目的id并将该选项插入数据库中
            setTitleId(titleEntity.getOptionList(), titleId);
        }
        return headlineId;
    }

    /**
     * 为每个选项设置所对应的题目的id值
     * 并插入数据库中
     *
     * @param optionList
     */
    private void setTitleId(List<option> optionList, int titleId) {
        for (option op : optionList) {
            //设置题目id
            op.setFk_title(titleId);
            //将该题目插入数据库中
            boolean flag = insertOption(op);
        }
    }

    /**
     * 通过用户id和投票标题内容查询
     * 指定标题是否存在数据库中
     *
     * @param userId
     * @param content
     * @return true表示存在；false表示不存在
     */
    @Override
    public boolean isContentExist(int userId, String content) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("content", content);
        int num = voteDocumentDao.isHeadlineIdExist(map);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 插入标题
     *
     * @param voteDocument
     * @return true表示插入数据成功；false表示插入数据失败
     */
    @Override
    public boolean insertVoteDocument(voteDocument voteDocument) {
        int num = voteDocumentDao.insertVoteDocument(voteDocument);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 通过用户id和标题内容
     * 查询出该标题的id
     *
     * @param
     * @return
     */
    @Override
    public int findHeadlineId(int userId, String content) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("content", content);
        int id = voteDocumentDao.findHeadlineIdByUserIdContent(map);
        return id;
    }

    /**
     * 通过标题id和题目内容查询是否存在该题目
     * true表示存在，false表示不存在
     *
     * @param titleContent
     * @param headlineId
     * @return
     */
    @Override
    public boolean isTitleContentExist(String titleContent, int headlineId) {
        Map<String, Object> map = new HashMap<>();
        map.put("titleContent", titleContent);
        map.put("headlineId", headlineId);
        int num = voteDao.isTitleIdExist(map);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 插入题目
     * titleContent headlineId
     *
     * @param vote
     * @return true表示插入题目成功，false表示失败
     */
    @Override
    public boolean insertVote(vote vote) {
        int num = voteDao.insertVote(vote);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 通过标题id和题目内容
     * 查询出指定题目的id值
     *
     * @param titleContent headlineId
     * @return
     */
    @Override
    public int findTitleId(String titleContent, int headlineId) {
        Map<String, Object> map = new HashMap<>();
        map.put("titleContent", titleContent);
        map.put("headlineId", headlineId);
        int id = voteDao.findTitleIdByTitleContentHeadlineId(map);
        return id;
    }

    /**
     * 通过题目id和选项内容查询
     * 判断指定选项是否存在数据库中
     *
     * @param option
     * @return
     */
    @Override
    public boolean isOptionIdExist(option option) {
        int num = optionDao.isOptionIdExist(option);
        if (num > 0) {
            return true;
        }
        return false;
    }

    /**
     * 插入选项的内容
     *
     * @param option
     * @return
     */
    @Override
    public boolean insertOption(option option) {
        int num = optionDao.insertOption(option);
        if (num > 0) {
            return true;
        }
        return false;
    }
}