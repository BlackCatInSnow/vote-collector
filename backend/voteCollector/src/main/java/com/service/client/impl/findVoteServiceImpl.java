package com.service.client.impl;

import com.dao.client.optionDao;
import com.dao.client.voteDao;
import com.dao.client.voteDocumentDao;
import com.entity.option;
import com.entity.packing.pageVote;
import com.entity.packing.titleEntity;
import com.entity.packing.voteEntity;
import com.entity.vote;
import com.entity.voteDocument;
import com.service.client.findVoteService;
import com.util.pageIndex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author CAIYUHUI
 * @create 2018/07/22 08:06
 **/
@Service("findVoteService")
public class findVoteServiceImpl implements findVoteService {

    @Autowired
    private voteDocumentDao voteDocumentDao;
    @Autowired
    private voteDao voteDao;
    @Autowired
    private optionDao optionDao;

    /**
     * 查询所有的投票
     *
     * @return
     */
    @Override
    public List<voteEntity> findAllVote(int index, int pageSize) {
        return findVoteByUserId(index, pageSize, 0);
    }

    /**
     * 查询指定用户id的所有投票
     *
     * @param index
     * @param pageSize
     * @param userId
     * @return
     */
    @Override
    public List<voteEntity> findVoteByUserId(int index, int pageSize, int userId) {
        List<voteEntity> voteEntityList = new ArrayList<>();

        voteEntity voteEntity = null;

        //查询所有标题
        List<voteDocument> voteDocuments = findVoteDocument(index, pageSize, userId);


        //遍历每一个标题，并包装
        for (voteDocument vd : voteDocuments) {
            voteEntity = new voteEntity();
            //封装到voteEntity类中
            voteEntity.setVoteDocument(vd);
            List<titleEntity> titleEntityList = new ArrayList<>();

            //根据标题的id值查询出相应的所有题目
            List<vote> votes = new ArrayList<>();
            votes = findVote(vd.getHeadlineId());

            titleEntity titleEntity = null;

            //遍历每一个题目
            for (vote vote : votes) {
                titleEntity = new titleEntity();
                titleEntity.setVote(vote);

                //根据题目的id查询出相应的所有选项
                List<option> optionList = findOption(vote.getTitleId());

                titleEntity.setOptionList(optionList);
                titleEntityList.add(titleEntity);
            }

            voteEntity.setTitleEntitiesList(titleEntityList);

            voteEntityList.add(voteEntity);
        }
        return voteEntityList;
    }

    /**
     * 分页显示所有
     *
     * @param index
     * @param pageSize
     * @param userId
     * @return
     */
    @Override
    public pageVote toPageVote(int index, int pageSize, int userId) {
        Map<String, Integer> map = pageIndex.getMap(index, pageSize);
        map.put("userId", userId);

        int count = voteDocumentDao.getCountVoteDocument(map);
        List<voteEntity> voteEntities = findVoteByUserId(index, pageSize, userId);
        int totalPage = (int) Math.ceil((double) count / pageSize);

        pageVote pageVote = new pageVote();
        pageVote.setCurrentIndex(index);
        pageVote.setPageSize(pageSize);
        pageVote.setTotalNum(count);
        pageVote.setTotalPage(totalPage);
        pageVote.setVoteEntity(voteEntities);

        return pageVote;
    }

    /**
     * 根据headlineId获取该投票的所有内容
     * @param headlineId
     * @return
     */
    @Override
    public voteEntity toVoteEntity(int headlineId) {
        voteEntity ve = new voteEntity();
        titleEntity te = null;
        List<titleEntity> titleEntityList = new ArrayList<>();
        voteDocument voteDocument = findVoteDocumentByHeadlineId(headlineId);
        ve.setVoteDocument(voteDocument);
        List<vote> votes = findVote(headlineId);
        for (vote v : votes) {
            te = new titleEntity();
            te.setVote(v);
            te.setOptionList(findOption(v.getTitleId()));
            titleEntityList.add(te);
        }
        ve.setTitleEntitiesList(titleEntityList);
        return ve;
    }

    @Override
    public List<voteDocument> findVoteDocument(int index, int pageSize, int userId) {
        Map<String, Integer> map = pageIndex.getMap(index, pageSize);
        map.put("userId", userId);
        return voteDocumentDao.findAllVoteDocument(map);
    }

    /**
     * 根据headlineId获取投票标题
     *
     * @param headlineId
     * @return
     */
    @Override
    public voteDocument findVoteDocumentByHeadlineId(int headlineId) {
        return voteDocumentDao.findVoteDocumentByHeadline(headlineId);
    }

    /**
     * 根据headlineId标题id值查询出相应的题目
     *
     * @param headlineId
     * @return
     */
    @Override
    public List<vote> findVote(int headlineId) {
        return voteDao.findVoteByHeadlineId(headlineId);
    }

    /**
     * 根据titleId题目id值查询出相应的选项
     *
     * @param titleId
     * @return
     */
    @Override
    public List<option> findOption(int titleId) {
        return optionDao.findOptionByTitleId(titleId);
    }


}
