package com.service.client;

import com.entity.option;
import com.entity.packing.voteEntity;
import com.entity.vote;
import com.entity.voteDocument;

import java.util.Map;

/**
 * @author CAIYUHUI
 * @create 2018/07/19 20:03
 * 创建投票
 **/
public interface createVoteService {

    /**
     * 创建投票
     *
     * @return
     */
    int createNewVote(voteEntity voteEntity);

    /**
     * 插入标题
     *
     * @param voteDocument
     * @return
     */
    boolean insertVoteDocument(voteDocument voteDocument);

    /**
     * 通过用户id和投票标题内容查询
     * 指定标题是否存在数据库中
     *
     * @param userId
     * @param content
     * @return
     */
    boolean isContentExist(int userId, String content);

    /**
     * 通过用户id和标题内容
     * 查询出该标题的id
     *
     * @param map
     * @return
     */
    int findHeadlineId(int userId, String content);

    boolean isTitleContentExist(String titleContent, int headlineId);

    /**
     * 插入题目
     *
     * @param vote
     * @return
     */
    boolean insertVote(vote vote);

    /**
     * 通过题目内容和标题的id
     * 查询出指定题目的id值
     *
     * @param map
     * @return
     */
    int findTitleId(String titleContent, int headlineId);

    /**
     * 通过题目id和选项内容查询
     * 判断指定选项是否存在数据库中
     *
     * @param option
     * @return
     */
    boolean isOptionIdExist(option option);

    /**
     * 插入选项内容
     * optionContent titleId
     *
     * @param option
     * @return
     */
    boolean insertOption(option option);

}
