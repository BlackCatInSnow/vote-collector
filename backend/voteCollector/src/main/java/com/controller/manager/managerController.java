package com.controller.manager;

import com.entity.user;
import com.service.client.userService;
import com.service.manager.managerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/23 17:01
 **/
@Controller
public class managerController {

    @Autowired
    private managerService managerService;
    @Autowired
    private userService userService;

    /**
     * 管理员登录
     *
     * @param phone
     * @param password
     * @return
     */
    @RequestMapping("managerLogin")
    public @ResponseBody
    String login(String phone, String password) {
        boolean flag = managerService.login(phone, password);
        if (flag) {
            return "true";
        } else {
            return "false";
        }
    }

    /**
     * 添加用户
     *
     * @param phone
     * @param password
     * @return
     */
    @RequestMapping("addUser")
    public @ResponseBody
    String register(String phone, String password) {
        boolean flag = managerService.register(phone, password);
        if (flag) {
            return "true";
        } else {
            return "false";
        }
    }

    /**
     * 浏览所有用户
     *
     * @return
     */
    @RequestMapping("findAllUser")
    public @ResponseBody
    List<user> findAllUser() {
        return managerService.findAllUser();
    }

    /**
     * 删除用户
     * @param phone
     * @return
     */
/*    @RequestMapping("deleteUser")
    public @ResponseBody
    String deleteUser(String phone) {
        user user = userService.findUserByPhone(phone);
        user.setStatus(0);
        boolean flag = userService.updateUserInfo(user);
        if (flag) {
            return "true";
        } else {
            return "false";
        }
    }*/
}
