package com.controller.client;

import com.entity.packing.enableParam;
import com.entity.packing.pageVote;
import com.entity.packing.scanVoteEntity;
import com.entity.packing.voteEntity;
import com.service.client.createVoteService;
import com.service.client.findVoteService;
import com.service.client.updateVoteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/22 19:50
 **/
@Controller
public class voteController {

    @Resource
    private findVoteService findVoteService;
    @Resource
    private createVoteService createVoteService;
    @Resource
    private updateVoteService updateVoteService;

    /**
     * 浏览所有投票及统计结果
     *
     * @return
     */
    @RequestMapping("scanAllVote")
    public @ResponseBody
    pageVote scanAllVote(int index) {
        int pageSize = 10;
        return findVoteService.toPageVote(index, pageSize, 0);
    }

    /**
     * 浏览指定用户的投票及统计结果
     *
     * @param
     * @param
     * @return
     */
    @RequestMapping("scanVote")
    public @ResponseBody
    pageVote scanVote(@RequestBody scanVoteEntity scanVoteEntity) {
        int pageSize = 10;
        return findVoteService.toPageVote(scanVoteEntity.getIndex(), pageSize, scanVoteEntity.getUserId());
    }

    /**
     * 根据headlineId获取投票的所有信息
     *
     * @param headlineId
     * @return
     */
    @RequestMapping("scanVoteByHeadlineId")
    public @ResponseBody
    voteEntity scanVoteByHeadlineId(@RequestBody int headlineId) {
        return findVoteService.toVoteEntity(headlineId);
    }

    /**
     * 创建,修改投票
     *
     * @param voteEntity
     */
    @RequestMapping("createVote")
    public @ResponseBody
    voteEntity createVote(@RequestBody voteEntity voteEntity) {

        //投票的id
        int headlineId = voteEntity.getVoteDocument().getHeadlineId();
        //新的headlineId
        int newHeadlineId = 0;
        //如果存在headlineId，则修改投票
        if (headlineId > 0) {
            //删除原来的投票
            updateVoteService.delete(headlineId);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String curTime = sdf.format(new Date());
            voteEntity.getVoteDocument().setCreateTime(curTime);
            //重新创建该投票
            newHeadlineId = createVoteService.createNewVote(voteEntity);

        } else {
            //创建投票
            newHeadlineId = createVoteService.createNewVote(voteEntity);
        }

        return findVoteService.toVoteEntity(newHeadlineId);
    }

    /**
     * 删除投票
     *
     * @param headlineId
     * @return
     */
    @RequestMapping("deleteVote")
    public @ResponseBody
    String deleteVote(int headlineId) {
        boolean flag = updateVoteService.deleteVote(headlineId);
        if (flag) {
            return "true";
        } else {
            return "false";
        }
    }

    /**
     * 参与投票
     *
     * @param
     * @return
     */
    @RequestMapping("voteFor")
    public @ResponseBody
    String voteFor(@RequestBody int[] optionIds) {
        for (int i = 0; i < optionIds.length; i++) {
            if (!updateVoteService.writeVote(optionIds[i])) {
                return "false";
            }
//            System.out.println(optionIds[i]);
        }
        return "true";
    }

    /**
     * 启用投票
     * 投票标题id(headlineId)
     * 投票的截止时间（deadline）
     */
    @RequestMapping("enable")
    public @ResponseBody
    String enable(@RequestBody enableParam enableParam) {
        boolean flag = updateVoteService.updateEnable(enableParam.getHeadlineId(), enableParam.getDeadline());
        if (flag) {
            return "true";
        } else {
            return "false";
        }
    }
}