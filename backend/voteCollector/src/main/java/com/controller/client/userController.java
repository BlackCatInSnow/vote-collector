package com.controller.client;

import com.entity.userClaim;
import com.service.client.userService;
import com.util.parseUserClaimJson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;
/**
 * @author CAIYUHUI
 * @create 2018/07/18 15:22
 **/
@Controller
public class userController {
    @Resource
    private userService userService;

    /**
     *  用户登录
     * @param userclaim
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "login",method = RequestMethod.POST)
    public @ResponseBody
    boolean Login(@RequestBody String userclaim) throws IOException {
        System.out.println(userclaim);
        userClaim uc = parseUserClaimJson.getUserClaim(userclaim);
        return userService.login(uc);
    }
}