import com.controller.client.voteController;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author CAIYUHUI
 * @create 2018/07/24 09:32
 **/
public class testVoteController {
    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private voteController getVoteController() {
        return (voteController) applicationContext.getBean("voteController");
    }

    /*@Test
    public void testVoteFor() {
        int[] optionIds = new int[]{1, 2, 3, 4, 5};
        ObjectMapper mapper = new ObjectMapper();
        String result = new String();
        try {
            result = mapper.writeValueAsString(optionIds);
        } catch (IOException e) {
            e.printStackTrace();
        }
        getVoteController().voteFor(result);
    }*/
}
