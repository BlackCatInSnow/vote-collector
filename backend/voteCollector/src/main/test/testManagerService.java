import com.entity.user;
import com.service.manager.managerService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/23 15:00
 **/
public class testManagerService {
    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private managerService getManagerService() {
        return (managerService) applicationContext.getBean("managerService");
    }

    @Test
    public void testRegister() {
        boolean flag = getManagerService().register("10000", "2333");
        if (flag) {
            System.out.println("success");
        } else {
            System.out.println("failure");
        }
    }

    @Test
    public void testLogin() {
        boolean flag = getManagerService().login("10000", "2333");
        if (flag) {
            System.out.println("success");
        } else {
            System.out.println("failure");
        }
    }

    @Test
    public void testFindAllUser() {
        List<user> users = getManagerService().findAllUser();
        if (users != null) {
            for (user u : users) {
                System.out.println(u);
            }
        } else {
            System.out.println("failure");
        }
    }
}
