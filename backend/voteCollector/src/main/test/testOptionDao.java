import com.dao.client.optionDao;
import com.entity.option;
import com.entity.vote;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/19 17:47
 **/
public class testOptionDao {

    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private optionDao getOptionDao() {
        return (optionDao) applicationContext.getBean("optionDao");
    }

    @Test
    public void testUpdateNumberByOptionId() {
        int num = getOptionDao().updateNumberByOptionId(1);
        if (num>0) {
            System.out.println("success");
        }else {
            System.out.println("failure");
        }
    }

    @Test
    public void testFindOptionByTitleId() {
        List<option> options = getOptionDao().findOptionByTitleId(6);
        if (options != null) {
            for (option op : options) {
                System.out.println(op);
            }
        } else {
            System.out.println("failure");
        }
    }

}
