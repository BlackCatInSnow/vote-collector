import com.dao.client.optionDao;
import com.dao.client.voteDocumentDao;
import com.service.client.updateVoteService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author CAIYUHUI
 * @create 2018/07/21 23:12
 **/
public class testUpdateVoteService {

    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private updateVoteService getUpdateVoteService() {
        return (updateVoteService) applicationContext.getBean("updateVoteService");
    }

    @Test
    public void testDeleteVote() {
        boolean flag = getUpdateVoteService().deleteVote(1);
        if (flag) {
            System.out.println("success");
        }
        else {
            System.out.println("failure");
        }
    }



}
