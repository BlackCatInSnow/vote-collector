import com.entity.option;
import com.entity.packing.titleEntity;
import com.entity.packing.voteEntity;
import com.entity.vote;
import com.entity.voteDocument;
import com.service.client.createVoteService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/21 17:26
 **/
public class testCreateVoteService {
    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private createVoteService getCreateVoteService() {
        return (createVoteService) applicationContext.getBean("createVoteService");
    }

    @Test
    public void testCreateNewVote() {
        voteDocument voteDocument=new voteDocument();
        voteDocument.setFk_userId(3);
        voteDocument.setContent("这是一条测试标题");
        voteDocument.setCreateTime("2018-03-04");
        voteDocument.setDeadline("2019-10-30");

        List<titleEntity> titleEntityList=new ArrayList<>();
        titleEntity titleEntity;
        for (int j=0;j<3;j++) {
            vote vote=new vote();
            vote.setTitleContent("这是一条测试题目"+j);

            List<option> optionList=new ArrayList<>();
            option option;
            for (int i=0;i<4;i++) {
                option=new option();
                option.setOptionContent("这是一条测试选项"+i);
                optionList.add(option);
            }
            titleEntity=new titleEntity();
            titleEntity.setVote(vote);
            titleEntity.setOptionList(optionList);

            titleEntityList.add(titleEntity);
        }

        voteEntity voteEntity=new voteEntity();
        voteEntity.setVoteDocument(voteDocument);
        voteEntity.setTitleEntitiesList(titleEntityList);
        getCreateVoteService().createNewVote(voteEntity);
        System.out.println("OK");
    }
}
