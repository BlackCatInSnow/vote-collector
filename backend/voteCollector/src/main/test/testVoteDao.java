import com.dao.client.voteDao;
import com.entity.vote;
import com.entity.voteDocument;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/19 10:22
 **/
public class testVoteDao {

    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private voteDao getVoteDao() {
        return (voteDao) applicationContext.getBean("voteDao");
    }

    @Test
    public void testFindVoteByHeadlineId() {
        List<vote> votes = getVoteDao().findVoteByHeadlineId(1);
        if (votes!=null) {
            for (vote v:votes) {
                System.out.println(v);
            }
        }
        else {
            System.out.println("failure");
        }
    }
}
