import com.entity.packing.voteEntity;
import com.service.client.findVoteService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/23 22:02
 **/
public class testFindVoteService {
    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() throws Exception {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private findVoteService getFindVoteService() {
        return (findVoteService) applicationContext.getBean("findVoteService");
    }

    @Test
    public void testFindVoteByUserId() {
        List<voteEntity> list = getFindVoteService().findVoteByUserId(1, 6, 3);
        for (voteEntity ve:list) {
            System.out.println(ve);
        }
    }
}
