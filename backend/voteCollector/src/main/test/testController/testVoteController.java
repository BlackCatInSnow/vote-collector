package testController;

import com.controller.client.voteController;
import com.entity.option;
import com.entity.packing.titleEntity;
import com.entity.packing.voteEntity;
import com.entity.vote;
import com.entity.voteDocument;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author CAIYUHUI
 * @create 2018/07/25 09:00
 **/
public class testVoteController {
    private ApplicationContext applicationContext;

    //在setUp这个方法里得到spring容器
    @Before
    public void setUp() {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }
/*
    private voteController getVoteController() {
        return (voteController) applicationContext.getBean("voteController");
    }

    @Test
    public void testScanAllVote() {
      //  List<voteEntity> list = getVoteController().scanAllVote(1);
        for (voteEntity ve : list) {
            System.out.println(ve);
        }
    }

    @Test
    public void testScanVote() {
        List<voteEntity> list = getVoteController().scanVote(1, 2);
        for (voteEntity ve : list) {
            System.out.println(ve);
        }
    }

    @Test
    public void testCreateVote() {
        List<titleEntity> titleEntityList = new ArrayList<>();

        voteEntity voteEntity = new voteEntity();

        voteDocument voteDocument = new voteDocument();
        voteDocument.setContent("哈哈哈哈哈哈哈11111");
        voteDocument.setFk_userId(3);
        voteDocument.setCreateTime("2018-07-27");
        voteDocument.setDeadline("2018-07-30");

        vote vote = new vote();
        vote.setTitleContent("吼吼吼吼吼吼吼吼吼11111");

        List<option> optionList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            option option = new option();
            option.setOptionContent("选择" + i);
            optionList.add(option);
        }

        titleEntity titleEntity = new titleEntity();
        titleEntity.setVote(vote);
        titleEntity.setOptionList(optionList);

        titleEntityList.add(titleEntity);

        voteEntity.setVoteDocument(voteDocument);
        voteEntity.setTitleEntitiesList(titleEntityList);

        List<voteEntity> result = getVoteController().createVote(voteEntity);
        if (result != null) {
            System.out.println("success");
        } else {
            System.out.println("failure");
        }
    }

    @Test
    public void testDeleteVote() {
        String flag = getVoteController().deleteVote(2);
        if (flag.equals("true")) {
            System.out.println("success");
        } else {
            System.out.println("failure");
        }
    }

    @Test
    public void testVoteFor() {
        int[] optionIds = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        String flag = getVoteController().voteFor(optionIds);
        if (flag.equals("true")) {
            System.out.println("success");
        } else {
            System.out.println("failure");
        }
    }

*/
}