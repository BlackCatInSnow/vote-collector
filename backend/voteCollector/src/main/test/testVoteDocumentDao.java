import com.dao.client.voteDocumentDao;
import com.entity.voteDocument;
import com.util.pageIndex;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


import java.util.List;
import java.util.Map;


/**
 * @author CAIYUHUI
 * @create 2018/07/18 17:57
 **/
public class testVoteDocumentDao {
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring-mybatis.xml");
    }

    private voteDocumentDao getVoteDocumentDao() {
        return (voteDocumentDao) applicationContext.getBean("voteDocumentDao");
    }

    @Test
    public void testFindAllVoteDocument() {
        Map<String, Integer> map = pageIndex.getMap(1, 5);
        map.put("userId",3);
        List<voteDocument> voteDocuments = getVoteDocumentDao().findAllVoteDocument(map);
        if (voteDocuments != null) {
            for (voteDocument v : voteDocuments) {
                System.out.println(v);
            }
        } else {
            System.out.println("failure");
        }
    }
/*
    @Test
    public void testFindVoteDocumentByPhone() {
        List<voteDocument> voteDocuments=getVoteDocumentDao().findVoteDocumentByPhone("15819575203");
        if (voteDocuments!=null) {
            for (voteDocument v:voteDocuments) {
                System.out.println(v);
            }
        }
        else {
            System.out.println("failure");
        }
    }

    @Test
    public void testFindUserIdByPhone() {
        int userId = getVoteDocumentDao().findUserIdByPhone("10086");
        if (userId>0) {
            System.out.println(userId);
        }
        else {
            System.out.println("failure");
        }
    }

    @Test
    public void testInsertVoteDocument() {
        voteDocument voteDocument=new voteDocument();
        voteDocument.setContent("IDEA测试时插入的一条数据.IDEA测试时插入的一条数据.IDEA测试时插入的一条数据");
        voteDocument.setCreateTime("2018-09-20");
        voteDocument.setDeadline("2018-10-30");

        voteDocumentDao voteDocumentDao = getVoteDocumentDao();
        int userId = voteDocumentDao.findUserIdByPhone("10010");

        user user=new user();
        user.setUserId(userId);

        voteDocument.setUser(user);

        int num = voteDocumentDao.insertVoteDocument(voteDocument);
        if (num>0) {
            System.out.println("插入成功！");
        }
        else {
            System.out.println("failure");
        }
    }

    @Test
    public void testUpdateVoteDocumentStatus() {
        int num = getVoteDocumentDao().updateVoteDocumentStatus("10086");
        if (num>0) {
            System.out.println("success");
        }else {
            System.out.println("failure");
        }
    }

    @Test
    public void testFindHeadlineIdByUserIdContent() {
        Map<String,Object> map=new HashMap<>();
        map.put("userId",Integer.parseInt("1"));
        map.put("content","标题1");
        int id = getVoteDocumentDao().findHeadlineIdByUserIdContent(map);
        if (id>0) {
            System.out.println(id);
        }
        else {
            System.out.println("failure");
        }
    }*/
}
