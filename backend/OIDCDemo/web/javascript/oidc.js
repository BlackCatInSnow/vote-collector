
let config ={
    authority: "http://localhost:5000",
    client_id: "js",
    redirect_uri: "http://localhost:5003/callback.html",
    silent_redirect_uri: "http://localhost:5003/refresh-token.html'
    response_type: "id_token token",
    scope:"openid profile api1",
    post_logout_redirect_uri : "http://localhost:5003/index.jsp",
    filterProtocolClaims: true
}

Oidc.Log.logger = console;
console.log(account);

let mgr = new Oidc.UserManager(config);

document.getElementById("login").addEventListener("click", login, false);
document.getElementById("logout").addEventListener("click", logout, false);
mgr.getUser().then((user)=>{
    if (user) {
        log("您已登录", user.profile);
    }
    else {
        log("您未登录");
    }
});

function login() {
    mgr.signinRedirect();
}

function logout() {
    mgr.signoutRedirect();
}
function getUser(callback) {
    mgr.getUser().then(callback);
}
mgr.events.addUserSignedOut(function () {
    alert("已经退出！");
    console.log('UserSignedOut：', arguments);
    mgr.removeUser();
    getUser(display);
});

function log() {
    document.getElementById('results').innerText = '';

    Array.prototype.forEach.call(arguments, (msg) => {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== 'string') {
            msg = JSON.stringify(msg, null, 2);
        }
        document.getElementById('results').innerHTML += msg + '\r\n';
    });
}