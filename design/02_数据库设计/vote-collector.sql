/*
 Navicat Premium Data Transfer

 Source Server         : MySQL 8.0.11
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : demo

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 25/07/2018 04:09:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_option
-- ----------------------------
DROP TABLE IF EXISTS `t_option`;
CREATE TABLE `t_option`  (
  `optionId` int(4) NOT NULL AUTO_INCREMENT,
  `optionContent` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `fk_title` int(4) NOT NULL,
  `number` int(4) NOT NULL,
  PRIMARY KEY (`optionId`) USING BTREE,
  INDEX `fk_title`(`fk_title`) USING BTREE,
  CONSTRAINT `t_option_ibfk_1` FOREIGN KEY (`fk_title`) REFERENCES `t_vote` (`titleid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_option
-- ----------------------------
INSERT INTO `t_option` VALUES (1, '选项A', 1, 0);
INSERT INTO `t_option` VALUES (2, '选项B', 1, 0);
INSERT INTO `t_option` VALUES (3, '选项C', 1, 0);
INSERT INTO `t_option` VALUES (4, '选项D', 1, 0);
INSERT INTO `t_option` VALUES (5, '新来的选项A', 2, 0);
INSERT INTO `t_option` VALUES (6, '新来的选项B', 2, 0);
INSERT INTO `t_option` VALUES (7, '新来的选项C', 2, 0);
INSERT INTO `t_option` VALUES (8, '新来的选项D', 2, 0);
INSERT INTO `t_option` VALUES (9, '这是一条测试选项0', 6, 0);
INSERT INTO `t_option` VALUES (10, '这是一条测试选项1', 6, 0);
INSERT INTO `t_option` VALUES (11, '这是一条测试选项2', 6, 0);
INSERT INTO `t_option` VALUES (12, '这是一条测试选项3', 6, 0);
INSERT INTO `t_option` VALUES (13, '这是一条测试选项0', 7, 0);
INSERT INTO `t_option` VALUES (14, '这是一条测试选项1', 7, 0);
INSERT INTO `t_option` VALUES (15, '这是一条测试选项2', 7, 0);
INSERT INTO `t_option` VALUES (16, '这是一条测试选项3', 7, 0);
INSERT INTO `t_option` VALUES (17, '这是一条测试选项0', 8, 0);
INSERT INTO `t_option` VALUES (18, '这是一条测试选项1', 8, 0);
INSERT INTO `t_option` VALUES (19, '这是一条测试选项2', 8, 0);
INSERT INTO `t_option` VALUES (20, '这是一条测试选项3', 8, 0);

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `userId` int(4) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `role` int(4) NULL DEFAULT NULL,
  `status` int(4) NULL DEFAULT NULL,
  `registerTime` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`userId`) USING BTREE,
  INDEX `userId`(`userId`) USING BTREE,
  INDEX `userId_2`(`userId`) USING BTREE,
  INDEX `userId_3`(`userId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, '12306', '23333', NULL, 0, 1, '2018-07-20');
INSERT INTO `t_user` VALUES (2, '15819575203', '666', NULL, 0, 1, '2018-08-20');
INSERT INTO `t_user` VALUES (3, '10010', '123', NULL, 0, 1, '2018-07-15');
INSERT INTO `t_user` VALUES (4, '10086', 'ICy5YqxZB1uWSwcVLSNLcA==', NULL, 0, 1, '2018-07-22 16:03:12');
INSERT INTO `t_user` VALUES (6, '10000', '9+C5VlQGdqEpdgo+rjCSlA==', NULL, 1, 1, '2018-07-23 15:23:27');

-- ----------------------------
-- Table structure for t_vote
-- ----------------------------
DROP TABLE IF EXISTS `t_vote`;
CREATE TABLE `t_vote`  (
  `titleId` int(4) NOT NULL AUTO_INCREMENT,
  `titleContent` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `fk_headlineId` int(4) NOT NULL,
  `type` int(4) NULL DEFAULT NULL COMMENT '0：单选；1：双选',
  PRIMARY KEY (`titleId`) USING BTREE,
  INDEX `fk_headlineId`(`fk_headlineId`) USING BTREE,
  CONSTRAINT `t_vote_ibfk_1` FOREIGN KEY (`fk_headlineId`) REFERENCES `t_votedocument` (`headlineid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_vote
-- ----------------------------
INSERT INTO `t_vote` VALUES (1, '题目1', 1, 0);
INSERT INTO `t_vote` VALUES (2, '题目2', 1, 0);
INSERT INTO `t_vote` VALUES (3, '题目3', 1, 0);
INSERT INTO `t_vote` VALUES (4, '题目4', 1, 0);
INSERT INTO `t_vote` VALUES (5, '吼吼吼吼吼吼吼吼吼', 2, 0);
INSERT INTO `t_vote` VALUES (6, '这是一条测试题目0', 4, 0);
INSERT INTO `t_vote` VALUES (7, '这是一条测试题目1', 4, 0);
INSERT INTO `t_vote` VALUES (8, '这是一条测试题目2', 4, 0);

-- ----------------------------
-- Table structure for t_votedocument
-- ----------------------------
DROP TABLE IF EXISTS `t_votedocument`;
CREATE TABLE `t_votedocument`  (
  `headlineId` int(4) NOT NULL AUTO_INCREMENT,
  `content` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `fk_userId` int(4) NOT NULL,
  `createTime` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `deadline` varchar(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `voteStatus` int(4) NOT NULL,
  `enabled` int(4) NOT NULL COMMENT '该投票是否启用（1：启用；0：未启用）',
  PRIMARY KEY (`headlineId`) USING BTREE,
  INDEX `votedocument_ibfk_1`(`fk_userId`) USING BTREE,
  INDEX `headlineId`(`headlineId`) USING BTREE,
  CONSTRAINT `t_votedocument_ibfk_1` FOREIGN KEY (`fk_userId`) REFERENCES `t_user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_votedocument
-- ----------------------------
INSERT INTO `t_votedocument` VALUES (1, '标题1', 1, '2018-07-09', '2018-08-09', 0, 0);
INSERT INTO `t_votedocument` VALUES (2, '标题2', 2, '2018-07-10', '2018-08-02', 1, 1);
INSERT INTO `t_votedocument` VALUES (3, 'IDEA测试时插入的一条数据.IDEA测试时插入的一条数据.IDEA测试时插入的一条数据', 3, '2018-09-20', '2018-10-30', 1, 0);
INSERT INTO `t_votedocument` VALUES (4, '这是一条测试标题', 3, '2018-03-04', '2019-10-30', 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
