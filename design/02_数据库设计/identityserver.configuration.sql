/*
 Navicat Premium Data Transfer

 Source Server         : MySQL 8.0.11
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : identityserver.configuration

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 25/07/2018 03:55:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for __efmigrationshistory
-- ----------------------------
DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE `__efmigrationshistory`  (
  `MigrationId` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ProductVersion` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`MigrationId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of __efmigrationshistory
-- ----------------------------
INSERT INTO `__efmigrationshistory` VALUES ('20180722112158_InitialIdentityServerConfigurationDbMigration', '2.1.1-rtm-30846');

-- ----------------------------
-- Table structure for apiclaims
-- ----------------------------
DROP TABLE IF EXISTS `apiclaims`;
CREATE TABLE `apiclaims`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ApiClaims_ApiResourceId`(`ApiResourceId`) USING BTREE,
  CONSTRAINT `FK_ApiClaims_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for apiresources
-- ----------------------------
DROP TABLE IF EXISTS `apiresources`;
CREATE TABLE `apiresources`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Enabled` bit(1) NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DisplayName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `IX_ApiResources_Name`(`Name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apiresources
-- ----------------------------
INSERT INTO `apiresources` VALUES (1, b'1', 'api1', 'My API', NULL);

-- ----------------------------
-- Table structure for apiscopeclaims
-- ----------------------------
DROP TABLE IF EXISTS `apiscopeclaims`;
CREATE TABLE `apiscopeclaims`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ApiScopeId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ApiScopeClaims_ApiScopeId`(`ApiScopeId`) USING BTREE,
  CONSTRAINT `FK_ApiScopeClaims_ApiScopes_ApiScopeId` FOREIGN KEY (`ApiScopeId`) REFERENCES `apiscopes` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for apiscopes
-- ----------------------------
DROP TABLE IF EXISTS `apiscopes`;
CREATE TABLE `apiscopes`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DisplayName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Required` bit(1) NOT NULL,
  `Emphasize` bit(1) NOT NULL,
  `ShowInDiscoveryDocument` bit(1) NOT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `IX_ApiScopes_Name`(`Name`) USING BTREE,
  INDEX `IX_ApiScopes_ApiResourceId`(`ApiResourceId`) USING BTREE,
  CONSTRAINT `FK_ApiScopes_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apiscopes
-- ----------------------------
INSERT INTO `apiscopes` VALUES (1, 'api1', 'My API', NULL, b'0', b'0', b'1', 1);

-- ----------------------------
-- Table structure for apisecrets
-- ----------------------------
DROP TABLE IF EXISTS `apisecrets`;
CREATE TABLE `apisecrets`  (
  `Expiration` datetime(6) NULL DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Type` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ApiResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ApiSecrets_ApiResourceId`(`ApiResourceId`) USING BTREE,
  CONSTRAINT `FK_ApiSecrets_ApiResources_ApiResourceId` FOREIGN KEY (`ApiResourceId`) REFERENCES `apiresources` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for clientclaims
-- ----------------------------
DROP TABLE IF EXISTS `clientclaims`;
CREATE TABLE `clientclaims`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Value` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientClaims_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientClaims_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for clientcorsorigins
-- ----------------------------
DROP TABLE IF EXISTS `clientcorsorigins`;
CREATE TABLE `clientcorsorigins`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Origin` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientCorsOrigins_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientCorsOrigins_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientcorsorigins
-- ----------------------------
INSERT INTO `clientcorsorigins` VALUES (1, 'http://localhost:5003', 4);

-- ----------------------------
-- Table structure for clientgranttypes
-- ----------------------------
DROP TABLE IF EXISTS `clientgranttypes`;
CREATE TABLE `clientgranttypes`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `GrantType` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientGrantTypes_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientGrantTypes_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientgranttypes
-- ----------------------------
INSERT INTO `clientgranttypes` VALUES (1, 'client_credentials', 1);
INSERT INTO `clientgranttypes` VALUES (2, 'implicit', 4);
INSERT INTO `clientgranttypes` VALUES (3, 'password', 2);
INSERT INTO `clientgranttypes` VALUES (4, 'hybrid', 3);
INSERT INTO `clientgranttypes` VALUES (5, 'client_credentials', 3);

-- ----------------------------
-- Table structure for clientidprestrictions
-- ----------------------------
DROP TABLE IF EXISTS `clientidprestrictions`;
CREATE TABLE `clientidprestrictions`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Provider` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientIdPRestrictions_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientIdPRestrictions_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for clientpostlogoutredirecturis
-- ----------------------------
DROP TABLE IF EXISTS `clientpostlogoutredirecturis`;
CREATE TABLE `clientpostlogoutredirecturis`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `PostLogoutRedirectUri` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientPostLogoutRedirectUris_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientPostLogoutRedirectUris_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientpostlogoutredirecturis
-- ----------------------------
INSERT INTO `clientpostlogoutredirecturis` VALUES (1, 'http://localhost:5002/signout-callback-oidc', 3);
INSERT INTO `clientpostlogoutredirecturis` VALUES (2, 'http://localhost:5003/index.html', 4);

-- ----------------------------
-- Table structure for clientproperties
-- ----------------------------
DROP TABLE IF EXISTS `clientproperties`;
CREATE TABLE `clientproperties`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Key` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientProperties_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientProperties_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for clientredirecturis
-- ----------------------------
DROP TABLE IF EXISTS `clientredirecturis`;
CREATE TABLE `clientredirecturis`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectUri` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientRedirectUris_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientRedirectUris_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientredirecturis
-- ----------------------------
INSERT INTO `clientredirecturis` VALUES (1, 'http://localhost:5002/signin-oidc', 3);
INSERT INTO `clientredirecturis` VALUES (2, 'http://localhost:5003/callback.html', 4);

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Enabled` bit(1) NOT NULL,
  `ClientId` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ProtocolType` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `RequireClientSecret` bit(1) NOT NULL,
  `ClientName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ClientUri` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `LogoUri` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `RequireConsent` bit(1) NOT NULL,
  `AllowRememberConsent` bit(1) NOT NULL,
  `AlwaysIncludeUserClaimsInIdToken` bit(1) NOT NULL,
  `RequirePkce` bit(1) NOT NULL,
  `AllowPlainTextPkce` bit(1) NOT NULL,
  `AllowAccessTokensViaBrowser` bit(1) NOT NULL,
  `FrontChannelLogoutUri` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `FrontChannelLogoutSessionRequired` bit(1) NOT NULL,
  `BackChannelLogoutUri` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `BackChannelLogoutSessionRequired` bit(1) NOT NULL,
  `AllowOfflineAccess` bit(1) NOT NULL,
  `IdentityTokenLifetime` int(11) NOT NULL,
  `AccessTokenLifetime` int(11) NOT NULL,
  `AuthorizationCodeLifetime` int(11) NOT NULL,
  `ConsentLifetime` int(11) NULL DEFAULT NULL,
  `AbsoluteRefreshTokenLifetime` int(11) NOT NULL,
  `SlidingRefreshTokenLifetime` int(11) NOT NULL,
  `RefreshTokenUsage` int(11) NOT NULL,
  `UpdateAccessTokenClaimsOnRefresh` bit(1) NOT NULL,
  `RefreshTokenExpiration` int(11) NOT NULL,
  `AccessTokenType` int(11) NOT NULL,
  `EnableLocalLogin` bit(1) NOT NULL,
  `IncludeJwtId` bit(1) NOT NULL,
  `AlwaysSendClientClaims` bit(1) NOT NULL,
  `ClientClaimsPrefix` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `PairWiseSubjectSalt` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `IX_Clients_ClientId`(`ClientId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES (1, b'1', 'client', 'oidc', b'1', NULL, NULL, NULL, NULL, b'1', b'1', b'0', b'0', b'0', b'0', NULL, b'1', NULL, b'1', b'0', 300, 3600, 300, NULL, 2592000, 1296000, 1, b'0', 1, 0, b'1', b'0', b'0', 'client_', NULL);
INSERT INTO `clients` VALUES (2, b'1', 'ro.client', 'oidc', b'1', NULL, NULL, NULL, NULL, b'1', b'1', b'0', b'0', b'0', b'0', NULL, b'1', NULL, b'1', b'0', 300, 3600, 300, NULL, 2592000, 1296000, 1, b'0', 1, 0, b'1', b'0', b'0', 'client_', NULL);
INSERT INTO `clients` VALUES (3, b'1', 'mvc', 'oidc', b'1', 'MVC Client', NULL, NULL, NULL, b'1', b'1', b'0', b'0', b'0', b'0', NULL, b'1', NULL, b'1', b'1', 300, 3600, 300, NULL, 2592000, 1296000, 1, b'0', 1, 0, b'1', b'0', b'0', 'client_', NULL);
INSERT INTO `clients` VALUES (4, b'1', 'js', 'oidc', b'1', '投票系统', NULL, NULL, NULL, b'1', b'1', b'0', b'0', b'0', b'1', NULL, b'1', NULL, b'1', b'0', 300, 3600, 300, NULL, 2592000, 1296000, 1, b'0', 1, 0, b'1', b'0', b'0', 'client_', NULL);

-- ----------------------------
-- Table structure for clientscopes
-- ----------------------------
DROP TABLE IF EXISTS `clientscopes`;
CREATE TABLE `clientscopes`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Scope` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientScopes_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientScopes_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientscopes
-- ----------------------------
INSERT INTO `clientscopes` VALUES (1, 'openid', 3);
INSERT INTO `clientscopes` VALUES (2, 'api1', 2);
INSERT INTO `clientscopes` VALUES (3, 'api1', 1);
INSERT INTO `clientscopes` VALUES (4, 'openid', 4);
INSERT INTO `clientscopes` VALUES (5, 'profile', 4);
INSERT INTO `clientscopes` VALUES (6, 'api1', 4);
INSERT INTO `clientscopes` VALUES (7, 'profile', 3);
INSERT INTO `clientscopes` VALUES (8, 'api1', 3);

-- ----------------------------
-- Table structure for clientsecrets
-- ----------------------------
DROP TABLE IF EXISTS `clientsecrets`;
CREATE TABLE `clientsecrets`  (
  `Expiration` datetime(6) NULL DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Type` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `ClientId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_ClientSecrets_ClientId`(`ClientId`) USING BTREE,
  CONSTRAINT `FK_ClientSecrets_Clients_ClientId` FOREIGN KEY (`ClientId`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clientsecrets
-- ----------------------------
INSERT INTO `clientsecrets` VALUES (NULL, 1, NULL, 'K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=', 'SharedSecret', 3);
INSERT INTO `clientsecrets` VALUES (NULL, 2, NULL, 'K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=', 'SharedSecret', 2);
INSERT INTO `clientsecrets` VALUES (NULL, 3, NULL, 'K7gNU3sdo+OL0wNhqoVWhr3g6s1xYv72ol/pe/Unols=', 'SharedSecret', 1);

-- ----------------------------
-- Table structure for identityclaims
-- ----------------------------
DROP TABLE IF EXISTS `identityclaims`;
CREATE TABLE `identityclaims`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `IdentityResourceId` int(11) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `IX_IdentityClaims_IdentityResourceId`(`IdentityResourceId`) USING BTREE,
  CONSTRAINT `FK_IdentityClaims_IdentityResources_IdentityResourceId` FOREIGN KEY (`IdentityResourceId`) REFERENCES `identityresources` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of identityclaims
-- ----------------------------
INSERT INTO `identityclaims` VALUES (1, 'sub', 1);
INSERT INTO `identityclaims` VALUES (2, 'name', 2);
INSERT INTO `identityclaims` VALUES (3, 'family_name', 2);
INSERT INTO `identityclaims` VALUES (4, 'given_name', 2);
INSERT INTO `identityclaims` VALUES (5, 'middle_name', 2);
INSERT INTO `identityclaims` VALUES (6, 'nickname', 2);
INSERT INTO `identityclaims` VALUES (7, 'preferred_username', 2);
INSERT INTO `identityclaims` VALUES (8, 'profile', 2);
INSERT INTO `identityclaims` VALUES (9, 'picture', 2);
INSERT INTO `identityclaims` VALUES (10, 'website', 2);
INSERT INTO `identityclaims` VALUES (11, 'gender', 2);
INSERT INTO `identityclaims` VALUES (12, 'birthdate', 2);
INSERT INTO `identityclaims` VALUES (13, 'zoneinfo', 2);
INSERT INTO `identityclaims` VALUES (14, 'locale', 2);
INSERT INTO `identityclaims` VALUES (15, 'updated_at', 2);

-- ----------------------------
-- Table structure for identityresources
-- ----------------------------
DROP TABLE IF EXISTS `identityresources`;
CREATE TABLE `identityresources`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Enabled` bit(1) NOT NULL,
  `Name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DisplayName` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Required` bit(1) NOT NULL,
  `Emphasize` bit(1) NOT NULL,
  `ShowInDiscoveryDocument` bit(1) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  UNIQUE INDEX `IX_IdentityResources_Name`(`Name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of identityresources
-- ----------------------------
INSERT INTO `identityresources` VALUES (1, b'1', 'openid', 'Your user identifier', NULL, b'1', b'0', b'1');
INSERT INTO `identityresources` VALUES (2, b'1', 'profile', 'User profile', 'Your user profile information (first name, last name, etc.)', b'0', b'1', b'1');

SET FOREIGN_KEY_CHECKS = 1;
