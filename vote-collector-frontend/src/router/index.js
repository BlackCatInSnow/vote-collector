import Vue from 'vue'
import Router from 'vue-router'
import MyVotes from '@/components/MyVotes'
import Index from '@/components/Index'
import EditVote from '@/components/EditVote'
import SignInSignUp from '@/components/SignInSignUp'
import MyProfile from '@/components/MyProfile'
import Help from '@/components/Help'
import VotePage from '@/components/VotePage'
import VoteResult from '@/components/VoteResult'
import SignCallback from '@/components/SignCallback'
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    }, {
      path: '/edit_vote',
      name: 'EditVote',
      component: EditVote,
    }, {
      path: '/sign',
      name: 'Sign',
      component: SignInSignUp
    }, {
      path: '/my_profile',
      name: 'MyProfile',
      component: MyProfile
    }, {
      path: '/my_votes',
      name: 'MyVotes',
      component: MyVotes
    }, {
      path: '/help',
      name:'Help',
      component:Help
    },{
      path:'/vote_page/:id',
      name:'VotePage',
      component:VotePage
    },{
      path:'/vote_result',
      name:'VoteResult',
      component:VoteResult
    },{
      path:'/SignCallback/:token',
      name:'SignCallback',
      component:SignCallback
    }
  ]
});
